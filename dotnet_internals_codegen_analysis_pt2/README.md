# .NET Internals, IL Generation & Code Analysis

## Content
* **Profiler example**: Contains a simple profiler implementation (written in older C++ standard). This subfolder also contains information on how to build a local version of the CoreCLR, build a profiler and run it together with either the `corerun` or the `dotnet` command.
* **Detecting data-races**: SharpDetect is available on my gitlab page: [SharpDetect v2](https://gitlab.com/acizmarik/sharpdetect), alternatively you may visit its previous (discontinued) [SharpDetect v1](https://gitlab.com/acizmarik/sharpdetect-1.0) version which provides more information about the tool but does not use the Profiling API yet.
   * This tool is not production-ready and is still in development => use on your own risk
   * Presented examples when bug-searching
      * **ProducerConsumer.cs**: regular, safe implementation => no data-race possible
      * **UnsafeTaskParallelLibrary.cs**: unsafe call to TPL => data-race on basically every access to the shared field
* **Slides.pdf**: Exported pdf from slides used during presentation

