using System;
using System.Threading.Tasks;

namespace UnsafeTaskParallelLibrary
{
    class Program
    {
		static int Field;
		
        static void Main(string[] args)
        {
            var task1 = Task.Run(() => 
			{
				for (int i = 0; i < 10; i++)
					Field = i;
			});
			
			var task2 = Task.Run(() => 
			{
				for (int i = 0; i < 10; i++)
					Field = Field * 2;
			});
			
			Task.WaitAll(task1, task2);
        }
    }
}
